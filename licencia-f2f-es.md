# Licencia de producción de pares feministas

Con la Licencia de producción de pares feministas (F2F) son libres de compartir la obra (copiarla, distribuirla, ejecutarla, o comunicarla públicamente) y de hacer obras derivada bajo las siguientes condiciones:
Atribución

## Atribución

Debes reconocer los créditos de la obra de la manera especificada por la o las autoras o las licenciantes (pero no de una manera que sugiera que tiene su apoyo o que apoyan el uso que hace de su obra).
Compartir bajo la misma licencia

## Compartir igual

Si modifican o transforman esta obra, o producen una obra derivada, sólo pueden distribuir la obra generada bajo una licencia idéntica a ésta.
Feminista no capitalista

## Feminista no capitalista

La explotación comercial de esta obra sólo está permitida a cooperativas, organizaciones y colectivos sin fines de lucro, y organizaciones de trabajadoras autogestionadas, que se identifiquen y organicen bajo principios feministas. Todo excedente o plusvalía obtenidos por el ejercicio de los derechos concedidos por esta licencia sobre la obra debe ser reinvertidos en la lucha contra el patriarcado y el capitalismo.
Entendiendo que…
- Alguna de estas condiciones pueden no aplicarse si se obtiene el permiso de la o las titulares de los derechos de autor.
- Cuando la obra o alguno de sus elementos se halle en el dominio público según la ley vigente aplicable, esta situación no quedará afectada por la licencia.
- Los derechos siguientes no quedan afectados por la licencia de ninguna manera:
  - Los derechos derivados de usos legítimos u otras limitaciones reconocidas por ley no se ven afectados por lo anterior;
  - Los derechos morales de la o las autoras;
  - Derechos que pueden ostentar otras personas sobre la propia obra o su uso, como por ejemplo derechos de imagen o de privacidad.
  - Al reutilizar o distribuir la obra, tienen que dejar muy en claro los términos de la licencia de esta obra. La mejor forma de hacerlo es enlazar a esta página.

## Preguntas frecuentes

**¿Qué se entiende por ‘principios feministas’?**
Eso lo decide cada persona, colectiva u organización. Cómo mínimo tiene que tener una aspiración de lucha contra el cisheteropatriarcado.

**¿Qué es el cisheteropatriarcado?**
Es el sistema de organización social que tiene al hombre cisheterosexual como centro y medida de todo; estableciendo relaciones de poder desiguales y sometimiento con el resto de seres humanos y la naturaleza.

##Algunas obras publicadas con esta licencia
- Fanzine: Cómo montar una servidora feminista con una conexión casera. Ver ↗
- Fanzine: Ciberfeminismo radiofónico. Miguitas para la producción de podcasts feministas libres. Ver ↗

## Imágenes para tu obra

Copia de aquí las imágenes para añadir esta licencia a tu obra.

- [Para web ↗](https://labekka.red/media/f2f-web.png)
- [Para imprimir ↗](https://labekka.red/media/f2f-imprimir.png)
- [Vectores (SVG) ↗](https://labekka.red/media/f2f-vectores.svg)

## Créditos

Esta licencia fue creada a partir de la [Licencia de Producción de Pares](https://endefensadelsl.org/ppl_deed_es.html).
