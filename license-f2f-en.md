# Feminist Peer Production Licence

Under the Feminist Peer Production License (F2F) **you are free to share the work** (copy, distribute, perform, or publicly communicate it) and **to make derivative works** under the following conditions:

## Attribution

You must **credit the work in the manner specified by the author(s) or licensor(s)** (but not in a way that suggests that they endorse or support your use of their work).

## Sharing under the same licence

If you modify or transform this work, or produce a derivative work, **you may only distribute the generated work under a licence identical to this one.**

## Non-capitalist feminism

Commercial exploitation of this work is only permitted to cooperatives, non-profit organisations and collectives, and self-managed workers' organisations, **which identify and organise themselves under feminist principles**. Any surplus or surplus value obtained from the exercise of the rights granted by this licence on the work must be reinvested in the struggle against patriarchy and capitalism.

## Understanding that...

- Some of these conditions may not apply if permission is obtained from the copyright holder(s)
- Where the work or any element of the work is in the public domain under applicable law, this is not affected by the license.
- The following rights are not affected by the license in any way:
    - Rights arising from legitimate uses or other limitations recognised by law are not affected by the above;
    - The moral rights of the author(s);
    - Rights that others may have in the work itself or its use, such as image or privacy rights.
    - When reusing or distributing the work, you need to make very clear the terms of the license for this work. The best way to do this is to link to this page
    - When reusing or distributing the work, you need to make very clear the terms of the license for this work. The best way to do this is to link to this page.
    

---

### Frequently Asked Questions

**What is meant by 'feminist principles'?**

That is up to each individual, collective or organisation to decide. At the very least it has to have an aspiration to fight against cisheteropatriarchy.

**What is cisheteropatriarchy?**

It is the system of social organisation that has the cisheterosexual men as the centre and measure of everything; establishing unequal power relations and oppression with the rest of human beings and nature.

### Some works published under this licence

- Fanzine: How to set up a feminist server with a home connection. [Link](https://labekka.red/servidoras-feministas)
- Fanzine: Ciberfeminismo radiofónico. Little crumbs for the production of free feminist podcasts. [Link](https://radioslibres.net/ciberfem)

### Images for your work

Copy from here the images to add this license to your work.

- [For web](https://labekka.red/media/f2f-web.png)
- [For print](https://labekka.red/media/f2f-imprimir.png)
- [Vectors (SVG)](https://labekka.red/media/f2f-vectores.svg)

### Credits

This license was created from [the Peer Production Licence.](https://endefensadelsl.org/ppl_deed_es.html)

la_bekka - hackfeminist space

hacklabfeminista[at]riseup.net
